/// Check a Luhn checksum.
pub fn is_valid(luhn: &str) -> bool {
    let luhn = luhn.replace(' ', "");
    if luhn.len() <= 1 || luhn.chars().any(|char| !char.is_numeric()) {
        return false;
    }

    let sum = luhn
        .chars()
        .filter_map(|char| char.to_digit(10))
        .rev()
        .enumerate()
        .map(calculate)
        .sum::<u32>();
    sum % 10 == 0
}

fn calculate((index, digit): (usize, u32)) -> u32 {
    if index % 2 == 0 {
        digit
    } else {
        let multiple = digit * 2;
        if multiple > 9 {
            multiple - 9
        } else {
            multiple
        }
    }
}
