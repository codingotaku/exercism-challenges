static mut PRIMES: Vec<u32> = Vec::<u32>::new();

pub fn nth(n: u32) -> u32 {
    if n == 0 {
        return 2;
    };
    unsafe {
        if let Some(prime) = PRIMES.get(n as usize) {
            return *prime;
        }

        for number in *PRIMES.last().unwrap_or(&2)..=u32::MAX {
            if is_prime(number) {
                PRIMES.push(number);
            }

            if PRIMES.len() as u32 == n + 1 {
                break;
            }
        }
        *PRIMES.get(n as usize).unwrap()
    }
}

fn is_prime(n: u32) -> bool {
    if n <= 1 {
        return false;
    }

    for a in 2..n / 2 + 1 {
        if n % a == 0 {
            return false;
        }
    }
    true
}
