use std::collections::HashSet;

fn word_to_sorted_chars(word: &str) -> Vec<char> {
    let mut chars = word.to_lowercase().chars().collect::<Vec<char>>();
    chars.sort_by(|prev, next| next.cmp(prev));
    chars
}

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    let chars = &word_to_sorted_chars(word);

    HashSet::from_iter(
        possible_anagrams
            .iter()
            .filter(|possible_anagram| {
                word.to_lowercase() != possible_anagram.to_lowercase()
                    && word_to_sorted_chars(possible_anagram).eq(chars)
            })
            .cloned(),
    )
}
