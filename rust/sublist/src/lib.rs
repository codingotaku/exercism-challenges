#[derive(Debug, PartialEq, Eq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

fn intersects<T: PartialEq>(main_list: &[T], list_to_check: &[T]) -> bool {
    list_to_check.is_empty()
        || main_list
            .windows(list_to_check.len())
            .any(|window| window.eq(list_to_check))
}

pub fn sublist<T: PartialEq>(first_list: &[T], second_list: &[T]) -> Comparison {
    let is_superlist = intersects(first_list, second_list);
    let is_sublist = intersects(second_list, first_list);

    match (is_superlist, is_sublist) {
        (true, true) => Comparison::Equal,
        (true, false) => Comparison::Superlist,
        (false, true) => Comparison::Sublist,
        (false, false) => Comparison::Unequal,
    }
}
