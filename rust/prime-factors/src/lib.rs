pub fn factors(mut number: u64) -> Vec<u64> {
    let mut prime_factors = Vec::<u64>::new();

    while number > 1 {
        let factor = (2..=number).find(|divisor| number % divisor == 0).unwrap();
        prime_factors.push(factor);
        number /= factor;
    }
    prime_factors
}
