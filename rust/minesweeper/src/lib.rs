const RELATIVE_NEIGHBOURS: [[i32; 2]; 8] = [
    [1, 0],
    [-1, 0],
    [1, 1],
    [0, 1],
    [-1, 1],
    [1, -1],
    [0, -1],
    [-1, -1],
];

#[derive(Copy, Clone, PartialEq)]
enum Tile {
    Empty,
    Mine,
    Flags(usize),
}

impl Tile {
    fn as_char(self) -> char {
        use Tile::*;
        match self {
            Empty => ' ',
            Mine => '*',
            Flags(count) => char::from_digit(count as u32, 10).unwrap(),
        }
    }

    fn get_incremented_tile(self) -> Self {
        use Tile::*;
        match self {
            Empty => Flags(1),
            Mine => Mine,
            Flags(count) => Flags(count + 1),
        }
    }
}

pub fn annotate(board: &[&str]) -> Vec<String> {
    let mut minefield: Vec<Vec<Tile>> = board
        .iter()
        .map(|row| {
            row.chars()
                .map(|ch| match ch {
                    '*' => Tile::Mine,
                    _ => Tile::Empty,
                })
                .collect()
        })
        .collect();

    let max_rows = minefield.len();
    let max_columns = if minefield.is_empty() {
        0
    } else {
        minefield[0].len()
    };

    for row in 0..max_rows {
        for column in 0..max_columns {
            if minefield[row][column] == Tile::Mine {
                for (neighbour_row, neighbour_column) in get_neighbours(
                    row as i32,
                    column as i32,
                    max_rows as i32,
                    max_columns as i32,
                ) {
                    minefield[neighbour_row][neighbour_column] =
                        minefield[neighbour_row][neighbour_column].get_incremented_tile();
                }
            }
        }
    }

    minefield
        .iter()
        .map(|row| row.iter().map(|tile| tile.as_char()).collect())
        .collect()
}

fn get_neighbours(row: i32, column: i32, max_rows: i32, max_columns: i32) -> Vec<(usize, usize)> {
    let mut neighbours = Vec::<(usize, usize)>::new();

    for [relative_row, relative_column] in RELATIVE_NEIGHBOURS {
        let neighbour_row = row + relative_row;
        let neighbour_column = column + relative_column;

        if neighbour_row >= 0
            && neighbour_row < max_rows
            && neighbour_column >= 0
            && neighbour_column < max_columns
        {
            neighbours.push((neighbour_row as usize, neighbour_column as usize));
        }
    }
    neighbours
}
