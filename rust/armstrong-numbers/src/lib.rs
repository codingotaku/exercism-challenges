pub fn is_armstrong_number(num: u32) -> bool {
    let num_string = num.to_string();
    let len = num_string.len() as u32;

    num as usize
        == num_string
            .chars()
            .map(|character| (character.to_digit(10).unwrap() as usize).pow(len))
            .sum()
}
