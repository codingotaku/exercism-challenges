use std::collections::BTreeMap;

pub fn raindrops(n: u32) -> String {
    let sound = BTreeMap::from([(3, "Pling"), (5, "Plang"), (7, "Plong")])
        .iter()
        .filter(|divisor_pair| n % divisor_pair.0 == 0)
        .map(|pair| pair.1.to_string())
        .collect::<Vec<String>>()
        .join("");

    if sound.is_empty() {
        n.to_string()
    } else {
        sound
    }
}
