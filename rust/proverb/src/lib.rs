pub fn build_proverb(list: &[&str]) -> String {
    let mut proverb = Vec::<String>::new();
    for (index, word) in list.iter().enumerate() {
        if index < list.len() - 1 {
            proverb.push(format!(
                "For want of a {} the {} was lost.",
                word,
                list[index + 1]
            ));
        } else {
            proverb.push(format!("And all for the want of a {}.", list[0]));
        }
    }
    proverb.join("\n")
}
