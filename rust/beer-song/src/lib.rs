fn bottles_left(n: u32) -> String {
    if n == 1 { "1 bottle".to_string() } else { format!("{} bottles",n) }
}

pub fn verse(n: u32) -> String {
    match n {
        0 => "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n".to_string(),
        1 => "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n".to_string(),
        _ => format!("{count} bottles of beer on the wall, {count} bottles of beer.\nTake one down and pass it around, {next_bottle} of beer on the wall.\n", count=n, next_bottle=bottles_left(n - 1))
    }
}

pub fn sing(start: u32, end: u32) -> String {
    (end..=start).rev().map(verse).collect::<Vec<String>>().join("\n")
}
