<?php

class HighSchoolSweetheart
{
    public function firstLetter(string $name): string
    {
        return trim($name)[0];
    }

    public function initial(string $name): string
    {
        $letter = strtoupper($this->firstletter($name));
        return "$letter.";
    }

    public function initials(string $name): string
    {
        return implode(" ",array_map([$this,'initial'], explode(" ",$name)));
    }

    public function pair(string $sweetheart_a, string $sweetheart_b): string
    {
    return "     ******       ******
   **      **   **      **
 **         ** **         **
**            *            **
**                         **
**     {$this->initials($sweetheart_a)}  +  {$this->initials($sweetheart_b)}     **
 **                       **
   **                   **
     **               **
       **           **
         **       **
           **   **
             ***
              *";
    }
}
