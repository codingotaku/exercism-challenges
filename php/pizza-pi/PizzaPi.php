<?php

class PizzaPi
{
    public function calculateDoughRequirement($pizzas, $persons)
    {
      return $pizzas * (($persons * 20) + 200);
    }

    public function calculateSauceRequirement($pizzas, $sause_can_volumn)
    {
        return $pizzas * 125 / $sause_can_volumn;
    }

    public function calculateCheeseCubeCoverage($side_length, $thickness, $diameter)
    {
        return floor(($side_length ** 3) / ($thickness * pi() * $diameter));
    }

    public function calculateLeftOverSlices($pizzas, $firends)
    {
        return $pizzas * 8 % $firends;
    }
}
