#!/usr/bin/env bash

[[ "$#" -lt 1 ]] && name="you" || name=$1

echo One for $name, one for me.
