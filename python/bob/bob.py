import re

def is_yelling(hey_bob):
    return hey_bob.isupper()

def is_question(hey_bob):
    return hey_bob.endswith("?")

def response(hey_bob):
    hey_bob = hey_bob.strip()

    if not hey_bob:
        return "Fine. Be that way!"

    if is_yelling(hey_bob):
        if is_question(hey_bob):
            return "Calm down, I know what I'm doing!"
        return "Whoa, chill out!"

    if is_question(hey_bob):
        return "Sure."

    return "Whatever."