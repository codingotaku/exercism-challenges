class Garden:
    DEFAULT_STUDENTS = ["Alice", "Bob", "Charlie", "David", "Eve", "Fred", "Ginny", "Harriet", "Ileana", "Joseph", "Kincaid", "Larry"]
    PLANTS = {
         "C": "Clover",
         "G": "Grass",
         "R": "Radishes",
         "V": "Violets"
        }
    def __init__(self, diagram, students=DEFAULT_STUDENTS):
        self.students = sorted(students)
        self.diagram = diagram.splitlines()
    
    def plants(self, student):
        _start = self.students.index(student) * 2
        _plants = list()
        for row in self.diagram :
            _plants.extend(row[_start : _start + 2])
        return list(map(self.PLANTS.get, _plants))




