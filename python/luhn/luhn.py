from re import match

class Luhn:
    def __init__(self, card_num):
        self.card_num = card_num.strip().replace(" ", "")


    def valid(self):
        print(self.card_num.isdigit())
        if len(self.card_num) <= 1 or not self.card_num.isdigit():
            return False
        numbers = [int(x) for x in self.card_num]
        for i in range(len(numbers)-2, -1, -2):
            num = numbers[i] * 2
            print(i, num, numbers)
            if num > 9:
                num -= 9
            numbers[i] = num
        return sum(numbers) % 10 == 0
