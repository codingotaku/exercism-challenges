from random import random
from math import floor

class Cipher:
    def __init__(self, key=None):
        self.start = ord('a')
        self.delta = ord('z') - self.start + 1
        self.key = key if key else self._gen_key(100)

    def _gen_key(self, length):
        return "".join([chr(floor(random() * self.delta) + self.start) for i in range(length)])

    def _fill_cipher(self, length):
        return [ord(self.key[i % len(self.key)]) - self.start for i in range(length)]

    def encode(self, text):
        cipher = self._fill_cipher(len(text))
        res = list()
        for index, char in enumerate(text):
            norm = ord(char) - self.start
            res.append(chr(((norm + cipher[index]) % self.delta) + self.start))
        return "".join(res)

    def decode(self, text):
        cipher = self._fill_cipher(len(text))
        res = list()
        for index, char in enumerate(text):
            norm = ord(char) - self.start
            res.append(chr(((norm - cipher[index] + self.delta) % self.delta) + self.start))
        return "".join(res)
