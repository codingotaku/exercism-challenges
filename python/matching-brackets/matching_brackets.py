BRACKETS = {
    ")":"(",
    "]":"[",
    "}":"{"
}

def is_paired(input_string):
    stack = []
    for c in input_string:
        if c in BRACKETS.values():
            stack.append(c)
        elif c in BRACKETS:
            if (not stack or stack.pop() != BRACKETS[c]):
                return False
    return len(stack) == 0
