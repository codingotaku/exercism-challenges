import re

def parse(markdown):
    # Wrap anythong that doesn't start with list or header in a paragraph
    markdown = re.sub("(^[^-|\\*|#].*)$","<p>\\1</p>", markdown, flags=re.M)

    # Generate Headers
    for i in range(6, 0, -1):
        markdown = re.sub(f"^{'#' * i}\\s(.*)$", f"<h{i}>\\1</h{i}>", markdown, flags=re.M)

    # generate text formatters
    markdown = re.sub("__([^__]*)__", "<strong>\\1</strong>",markdown, flags=re.M)
    markdown = re.sub("_([^_]*)_", "<em>\\1</em>",markdown, flags=re.M)

    # generate lists
    markdown = re.sub("^\\*\\s(.*)$", "<li>\\1</li>",markdown, flags=re.M)
    markdown = re.sub("(<li>.*</li>)","<ul>\\1</ul>", markdown, flags=re.S)

    return markdown.replace('\n','') # Clean-up linebreaks becuase the HTML doesn't expect it for some reason.
