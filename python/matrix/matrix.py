class Matrix:
    def __init__(self, matrix_string):
        self.matrix = []
        tmp = matrix_string.split("\n")
        for i in tmp:
            self.matrix.append( [int(item) for item in i.split(" ") ])

    def row(self, index):
        return self.matrix[index-1]

    def column(self, index):
        return [row[index-1] for row in self.matrix]
