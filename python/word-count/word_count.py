import re
def count_words(sentence):
    sentence = sentence.casefold()
    matches = re.findall("([a-z0-9]+'[a-z]{1,3}|[a-z0-9]+)", sentence)
    result = {}
    for x in set(matches):
        result[x]=matches.count(x)
    return result
