from operator import attrgetter
class Student:
    def __init__(self, name, grade):
        self._name: Final = name
        self._grade: Final = grade
    def name(self):
        return self._name
    def grade(self):
        return self._grade

class School:
    def __init__(self):
        self.students = []

    def add_student(self, name, grade):
        self.students.append(Student(name, grade))
        self.students.sort(key= lambda x: [x.grade(), x.name()])

    def roster(self):
        return [x.name() for x in self.students]

    def grade(self, grade_number):
        return [x.name() for x in self.students if x.grade() == grade_number]
