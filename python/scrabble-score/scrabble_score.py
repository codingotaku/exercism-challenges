table = {1:['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'],
2:['D', 'G'],
3:['B', 'C', 'M', 'P'],
4:['F', 'H', 'V', 'W', 'Y'],
5:['K'],
8:['J', 'X'],
10:['Q', 'Z']
}
def score(word):
    count = 0
    for i in word:
        count += [x for x in table if i.upper() in table[x]][0]
    return count
