from operator import itemgetter
import json

class RestAPI:
    def __init__(self, database={"users": []}):
        self.database = database
    
    def _sort_response(self, users):
        users.sort(key=itemgetter("name"))
        return json.dumps({"users": users})
    
    def _get_balance(self, user):
        owed_by = sum(user["owed_by"].values())
        owe = sum(user["owes"].values())
        return owed_by - owe
    
    def _cleanup(self, lender, borrower_name):        
        if borrower_name in lender["owed_by"] and borrower_name in lender["owes"]:
            if lender["owed_by"][borrower_name] == lender["owes"][borrower_name]:
                del lender["owed_by"][borrower_name]
                del lender["owes"][borrower_name]

        if borrower_name in lender["owes"] and lender["owes"][borrower_name] == 0:
            del lender["owes"][borrower_name]
        if borrower_name in lender["owed_by"] and lender["owed_by"][borrower_name] == 0:
            del lender["owed_by"][borrower_name]

    def _get_users(self, payload=None):
        if not payload:
            return json.dumps(self.database)
        payload = json.loads(payload)
        users = [x for x in self.database["users"] if x["name"] in payload["users"]]
        return self._sort_response(users)

    def _add_user(self, payload):
        if not payload:
            return
        payload = json.loads(payload)
        entry = {"name": payload["user"], "owes": {}, "owed_by": {}, "balance": 0.0}
        self.database["users"].append(entry)
        return json.dumps(entry)
    
    def _iou(self, payload):
        if not payload:
            return
        payload = json.loads(payload)

        # Assume that both lender and borrower are always present in the database
        lender_name = payload["lender"]
        borrower_name = payload["borrower"]
        amount = payload["amount"]

        lender = next(n for n in self.database["users"] if n["name"] == lender_name)
        borrower = next(n for n in self.database["users"] if n["name"] == borrower_name)

        if borrower_name in lender["owes"]:
            if lender["owes"][borrower_name] - amount < 0:
                lender["owed_by"][borrower_name] = amount - lender["owes"][borrower_name]
                del lender["owes"][borrower_name]
            else:
                lender["owes"][borrower_name] -= amount
                if lender["owes"][borrower_name] == 0:
                    del lender["owes"][borrower_name]
        else:
            lender["owed_by"][borrower_name] = amount
        
        if lender_name in borrower["owed_by"]:
            if borrower["owed_by"][lender_name] - amount < 0:
                borrower["owes"][lender_name] = amount - borrower["owed_by"][lender_name]
                del borrower["owed_by"][lender_name]
            else:
                borrower["owed_by"][lender_name] -= amount
                if borrower["owed_by"][lender_name] == 0:
                    del borrower["owed_by"][lender_name]
        else:
            borrower["owes"][lender_name] = amount
        
        lender["balance"] = self._get_balance(lender)
        borrower["balance"] = self._get_balance(borrower)
        return self._sort_response([lender, borrower])

    def get(self, url, payload=None):
        if url == "/users":
            return self._get_users(payload)

    def post(self, url, payload=None):
        if url == "/add":
            return self._add_user(payload)
        if url == "/iou":
            return self._iou(payload)
