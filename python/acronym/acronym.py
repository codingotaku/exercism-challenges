import re
def abbreviate(words):
    ls = re.findall("[a-zA-Z']+", words)
    return "".join([x[0].upper() for x in ls])
