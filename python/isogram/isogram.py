def is_isogram(string):
    val = string.replace("-","").replace(" ","").casefold()
    return len(set(val)) == len(val)
