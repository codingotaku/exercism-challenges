from operator import itemgetter

class Board:
    def __init__(self):
        self.teams = []

    def get(self, name):
        if not self[name]:
            team = Team(name)
            self.teams.append(team)
            return team
        return self[name]
    
    def __getitem__(self, name):
        return next((team for team in self.teams if team.name == name), None)
    
    def update_result(self, result, team1, team2):
        if result == 'win':
            team1.win()
            team2.lose()
        elif result == 'loss':
            team1.lose()
            team2.win()
        else:
            team1.draw()
            team2.draw()

    def results(self):
        header = f"{'Team':<31}| MP |  W |  D |  L |  P"
        table = list(map(str,sorted(self.teams,key=lambda x: [-x.p, x.name])))
        table.insert(0, header)
        return table
        

class Team:
    def __init__(self, name):
        self.name = name
        self.mp = self.w = self.d = self.l = self.p = 0

    def win(self):
        self.mp += 1
        self.w += 1
        self.p += 3

    def draw(self):
        self.mp += 1
        self.p += 1
        self.d += 1

    def lose(self):
        self.mp += 1
        self.l += 1

    def __repr__(self):
        return f"{self.name:<31}|{self.mp:>3} |{self.w:>3} |{self.d:>3} |{self.l:>3} |{self.p:>3}"


def tally(rows):
    board = Board()
    for row in rows:
        t1, t2, result = row.split(';')
        team1 = board.get(t1)
        team2 = board.get(t2)
        board.update_result(result, team1, team2)

    return board.results()
