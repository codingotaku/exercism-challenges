import {colorCode} from "./resistor-color"

export const decodedValue = (values) => Number(`${colorCode(values[0])}${colorCode(values[1])}`);
