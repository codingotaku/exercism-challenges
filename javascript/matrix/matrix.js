export class Matrix {
  constructor(matrix) {
    this._matrix = matrix.split("\n").map(row=> row.split(" ").map(Number));
    this._transpose = this._matrix[0].map((_,index) => this._matrix.map(row=> row[index]))
  }

  get rows() {
    return this._matrix;
  }

  get columns() {
    return this._transpose;
  }
}
