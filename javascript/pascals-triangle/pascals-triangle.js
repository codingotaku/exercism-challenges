
/**
 * 
 * @param {Number} count 
 * @returns {Array<Array<Number>>} Array representation of pascals triangle
 */
export const rows = (count) => {
  let pascals = [];
  for (let i = 0; i<count; i++) {
    pascals[i]=[];
    for(let j=0; j<=i; j++) {
      if(j>0 && j<i){
        pascals[i][j] = pascals[i-1][j-1] + pascals[i-1][j]
      }
      else {
        pascals[i][j] = 1;
      }
    }
  }
  return pascals;
};
