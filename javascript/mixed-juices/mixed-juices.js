// @ts-check

const SIZE_WEDGE_MAP = {
  'small': 6,
  'medium': 8,
  'large': 10
};

/**
 * Determines how long it takes to prepare a certain juice.
 *
 * @param {string} name
 * @returns {number} time in minutes
 */
export function timeToMixJuice(name) {
  let time;
  switch(name) {
    case 'Pure Strawberry Joy':
      time = 0.5;
      break;
    case 'Energizer':
      time = 1.5;
      break;
    case  'Green Garden':
      time = 1.5;
      break;
    case 'Tropical Island':
      time = 3;
      break;
    case 'All or Nothing':
      time = 5;
      break;
    default:
      time = 2.5;
      break;
  }
  return time;
}

/**
 * Calculates the number of limes that need to be cut
 * to reach a certain supply.
 *
 * @param {number} wedgesNeeded
 * @param {string[]} limes
 * @returns {number} number of limes cut
 */
export function limesToCut(wedgesNeeded, limes) {
  let index = 0;
  while(wedgesNeeded > 0 && index < limes.length) {
    wedgesNeeded -= SIZE_WEDGE_MAP[limes[index++]];
  }
  return index;
}

/**
 * Determines which juices still need to be prepared after the end of the shift.
 *
 * @param {number} timeLeft
 * @param {string[]} orders
 * @returns {string[]} remaining orders after the time is up
 */
export function remainingOrders(timeLeft, orders) {
  let index = 0;
  while(timeLeft>0 && orders.length > -1) {
    timeLeft -= timeToMixJuice(orders[index++]);
  }
  orders.splice(0,index);
  return orders;
}
