//
// This is only a SKELETON file for the 'Grade School' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

class Student {
  #name = null;
  #grade = null;
  constructor(name, grade) {
    this.#name = name;
    this.#grade = grade;
  }
  get name() {
    return this.#name;
  }
  get grade() {
    return this.#grade;
  }

  set grade(grade) {
    this.#grade = grade; 
  }
}


export class GradeSchool {
  #students = [];

  #sortStudents() {
   this.#students.sort((a, b) => a.grade - b.grade || a.name.localeCompare(b.name));
  }

  roster() {
    this.#sortStudents();

    const _roster = {};
    this.#students.forEach(student=>{
      if(!_roster[student.grade]){
        _roster[student.grade] = [];
      }
      _roster[student.grade].push(student.name);
    });
    return _roster;
  }

  add(name, grade) {
    const student = this.#students.find(student=> student.name === name);
    if (student !== undefined) {
      student.grade = grade;
    } else {
      this.#students.push(new Student(name, grade))
    }
  }

  grade(grade) {
    return this.#students.filter(student => student.grade === grade)
      .map(student => student.name)
      .sort();
  }
}
