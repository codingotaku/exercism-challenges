// List of all the 26 english alphabets
const ALPHABETS = [...'abcdefghijklmnopqrstuvwxyz']

/**
 * Checks if the given sentence is a pangram
 * @param {String} sentence to test
 * @returns {boolean} true if the sentence is a pangram
 */
export const isPangram = (sentence) => {
  const list = sentence.toLowerCase();
  return ALPHABETS.every(alphabet=> list.includes(alphabet));
};
