// This is only a SKELETON file for the 'Robot Name' exercise. It's been
// provided as a convenience to get your started writing code faster.

const OLD_NAMES = new Set();

export class Robot {
    #name = null;

    #genName() {
        const letters = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 2).toUpperCase();
        const digits = Math.random().toFixed(3);
        return letters + digits.substr(2);
    }
    constructor() {
        this.reset();
    }
    reset() {
        let name = this.#genName();
        while(OLD_NAMES.has(name)) {
            name = this.#genName()
        }
        OLD_NAMES.add(name);
        this.#name = name;
    }

    get name() {
        return this.#name;
    }
}

Robot.releaseNames = () => {OLD_NAMES.clear()};
