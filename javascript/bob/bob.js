/**
 * Checkes if the message contains all Capital letters.
 * @param {String} message 
 * @returns {Boolean} true if message is yelling
 */
function isYelling(message) {
  return /^[^a-z]*[A-Z][^a-z]*$/.test(message);
}

/**
 * Checkes if the message is a question.
 * @param {String} message 
 * @returns {Boolean} true if the message is a question.
 */
function isQuestion(message) {
  return message.endsWith("?")
}

/**
 * Bob's respons for the given message.
 * @param {String} message 
 * @param {String} Bob's response
 */
export const hey = (message) => {
  const normalized = message.replace(/\s/g,"");

  if(normalized.length === 0) {
    return "Fine. Be that way!";
  }
  if (isYelling(normalized)) {
    if (isQuestion(normalized)) {
      return "Calm down, I know what I'm doing!"
    }
    return "Whoa, chill out!"
  }
  if(isQuestion(normalized)) {
    return "Sure.";
  }

  return "Whatever."
};
