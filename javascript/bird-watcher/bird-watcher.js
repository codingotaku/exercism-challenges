// @ts-check

/**
 * Calculates the total bird count.
 *
 * @param {number[]} birdsPerDay
 * @returns {number} total bird count
 */
export function totalBirdCount(birdsPerDay) {
  let total = 0;
  birdsPerDay.forEach(bird=>total+=bird);
  return total;
}

/**
 * Calculates the total number of birds seen in a specific week.
 *
 * @param {number[]} birdsPerDay
 * @param {number} week
 * @returns {number} birds counted in the given week
 */
export function birdsInWeek(birdsPerDay, week) {
  let total = 0;
  const start = (week-1) *7;
  for (let index = start; index < start + 7; index++) {
    total += birdsPerDay[index];
  }
  return total;
}

/**
 * Fixes the counting mistake by increasing the bird count
 * by one for every second day.
 *
 * @param {number[]} birdsPerDay
 * @returns {number[]} corrected bird count data
 */
export function fixBirdCountLog(birdsPerDay) {
  for(let index = 0; index< birdsPerDay.length; index++) {
    if(index % 2 === 0) {
      birdsPerDay[index]++;
    }
  }
  return birdsPerDay;
}
