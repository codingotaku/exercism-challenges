/**
 * Returns a random string to use for Cipher key.
 * @returns {String} A random string of length 100
 */
const generateKey = ()=>{
  let key = "";
  for (let i = 0; i < 100; i++) {
    key += String.fromCharCode(Math.floor(Math.random() * 26) + 97);
  }
  return key;
}

export class Cipher {
  #cipher_key = null;
  // If someone doesn't submit a key at all, generate a truly random key of at least 100 lowercase characters in length.
  constructor(key=generateKey()) {
    this.#cipher_key = key;
    this.start = 'a'.charCodeAt(); // Smallest char position in ascii (a) -> 97
    this.delta = 'z'.codePointAt() - this.start + 1; // Total usable char length (a to z) - 26
  }

  /**
   * Generates an array full of cipher key with given length. 
   * If the given length is greater than the total keys, the array will repeatedly be filled with the keys till it is full.
   * @param {Number} length length of the string to encode/decode
   * @returns {Array<String>} Array of chars filled with the cipher key.
   */
  #fillCipher(length) {
    return Array(length)
    .fill()
    .map((_,i)=> this.#cipher_key[i % this.#cipher_key.length].charCodeAt(0) - this.start);
  }

  /**
   * Encodes the given text with a cipher key
   * @param {String} text 
   * @returns {String} Encoded string
   */
  encode(text) {
    const cipher = this.#fillCipher(text.length)
    const code = [];
    text.split('').map((c,i) => {
      const norm = c.charCodeAt() - this.start;
      const char = (norm + cipher[i]) % this.delta;
      code.push(String.fromCharCode(char + this.start));
    })
    return code.join('');
  }

  /**
   * Decodes the given text using cipher key
   * @param {String} text
   * @returns {String} decoded string
   */
  decode(code) {
    const cipher = this.#fillCipher(code.length)
    const text = [];
    code.split('').map((c,i) => {
      const norm = c.charCodeAt(0) - this.start;
      const char = (norm - cipher[i] + this.delta) % this.delta;
      text.push(String.fromCharCode((char + this.start)));
    })
    return text.join('');
  }

  get key() {
    return this.#cipher_key;
  }
}
