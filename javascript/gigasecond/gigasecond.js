const GIGASECOND_IN_MS = 10 ** 12;

/**
 * For a given moment, returns the moment that would be after a gigasecond has passed.
 * @param {Date} moment 
 * @returns {Date} moment after 1 gigasecond was passed
 */
export const gigasecond = (moment) => {
  return new Date(moment.getTime() + GIGASECOND_IN_MS);
};
