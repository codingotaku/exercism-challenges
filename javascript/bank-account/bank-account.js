export class BankAccount {
  constructor() {
    this._isOpen = false;
    this._balance = 0;
  }

  _verifyAccount() {
    if(!this.isOpen) {
      throw new ValueError("Account is not open");
    }
  }
  _verifyAmount(amount) {
    if(amount <= 0) {
      throw new ValueError("Amount must be positive"); 
    }
  }
  open() {
    if(this.isOpen) {
      throw new ValueError("Account is already open");
    }
    this.isOpen = true;
  }

  close() {
    this._verifyAccount();
    this.isOpen = false;
    this._balance = 0;
  }

  deposit(amount) {
    this._verifyAccount();
    this._verifyAmount(amount);
    this._balance += amount
  }

  withdraw(amount) {
    this._verifyAccount();
    this._verifyAmount(amount);
    this._verifyAmount(this._balance + 1 - amount);
    this._balance -= amount;
  }

  get balance() {
    this._verifyAccount();
    return this._balance;
  }
}

export class ValueError extends Error {
  constructor() {
    super('Bank account error');
  }
}
