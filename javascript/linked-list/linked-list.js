//
// This is only a SKELETON file for the 'Linked List' exercise. It's been provided as a
// convenience to get you started writing code faster.
//
class Node {
  constructor(value, previous, next) {
    this.value = value;
    this.previous = previous;
    this.next = next;
  }
}

export class LinkedList {
  #head = null;
  #tail = null;
  clear() {
    this.#head = null;
    this.#tail = null;
  }
  push(value) {
    const element = new Node(value, this.#tail)
    if(this.#tail) {
      this.#tail.next = element;
    } else {
      this.#head = element;
    }
    this.#tail = element;
  }

  pop() {
    if (!this.#tail) {
      throw new Error("List is empty");
    }
    const value = this.#tail.value;

    if (this.#tail.previous) {
      this.#tail = this.#tail.previous;
      this.#tail.next = null;
    } else {
      this.clear();
    }
    return value;
  }

  shift() {
    if (!this.#head) {
      throw new Error("List is empty");
    }
    
    const value = this.#head.value;
    if(this.#head.next) {
      this.#head = this.#head.next;
      this.#head.previous = null;
    } else {
      this.clear();
    }
    return value;
  }

  unshift(value) {
    const element = new Node(value, null, this.#head);
    if(this.#head) {
      this.#head.previous = element;
    } else {
      this.#tail = element;
    }
    this.#head = element
  }

  delete(value) {
    let element = this.#head;
    while(element) {
      if(element.value === value) {
        if(element.previous) {
          element.previous.next = element.next;
        } else {
          this.#head = element.next;
        }
        if(element.next) {
          element.next.previous = element.previous
        } else {
          this.#tail = element.previous;
        }
        break;
      }
      element = element.next;
    }
  }

  count() {
    let count = 0;
    let element = this.#head;
    while(element) {
      element = element.next;
      count++;
    }
    return count;
  }
}
