const EARTH_YEAR = 60 * 60 * 24 * 365.25; // Approximate, because leap seconds.

// Approximate yearth years for each planet in our solarsystem
const PLANET_YEAR_MAP = {
    mercury:0.2408467,
    venus:0.61519726,
    earth:1.0,
    mars:1.8808158,
    jupiter:11.862615,
    saturn:29.447498,
    uranus:84.016846,
    neptune:164.79132
}

/**
 * For the given age in seconds, calculates how old someone would be on in the given planet.
 * @param {String} planet Name of the planet in lowercase
 * @param {Number} seconds time in seconds to be converted to age
 * @returns Number of years for the given second on the secified planet.
 */
export const age = (planet, seconds) => {
  const planetYear = seconds / PLANET_YEAR_MAP[planet] / EARTH_YEAR;
  return Math.round (planetYear * 100) / 100; // Number.toFixed() sometimes has rounding issues
};
