const DNA_TO_RNA_MAP = {
    G:'C',
    C:'G',
    T:'A',
    A:'U'
}

/**
 * For a given DNA strand, returns its RNA complement (per RNA transcription).
 * @param {String} dna DNA strand
 * @returns {String} RNA strand
 */
export const toRna = (dna) => {
  return dna.split('').map(nucleotide => DNA_TO_RNA_MAP[nucleotide]).join('');
};
